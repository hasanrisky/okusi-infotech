<?php
  session_start();
 
  //Define Language file paths
  define("LANG_ID_PATH", $_SERVER['DOCUMENT_ROOT'] . '/includes/lang/id/');
  define("LANG_EN_PATH", $_SERVER['DOCUMENT_ROOT'] . '/includes/lang/en/');
 
 
  if (isset($_GET['lang'])) {
     
    // GET request found
 
    if ($_GET['lang'] == 'en') {
       
      // asked for the language 'en' so include the 'en.php' file
      include LANG_EN_PATH . 'en.php';
      $_SESSION['lang'] = 'en';
    } 
    else {
 
      // if not asked for 'id', include 'id.php' as default
      include LANG_ID_PATH . 'id.php';
      $_SESSION['lang'] = 'id';
    }
  } 
  
  else if (isset($_SESSION['lang'])) {
 
    //SESSION variable found
 
    if ($_SESSION['lang'] == 'id') {
 
      // language already set to 'id', so include 'id.php'
      include LANG_ID_PATH . 'id.php';
    } 
    else {
 
      // SESSION variable not set to 'id', so include 'en.php' by default
      include LANG_EN_PATH . 'en.php';
    }
  } 
  
  else {
     
    // SESSION varibale not set, so set it to 'id' and include 'id.php' by default
    include LANG_ID_PATH . 'id.php';
    $_SESSION['lang'] = 'id';
  }
?>



<?php $cssVersion = "3.4.2"; ?>


<title><?php print $PAGE_TITLE;?></title>

<?php if ($CURRENT_PAGE == "Index") { ?>
	<meta name="description" content="" />
	<meta name="keywords" content="" /> 
<?php } ?>
        <link rel="icon" href="<?php echo $base_url; ?>/assets/img/okusi-infotech.ico">
        <link type="text/plain" rel="author" href="<?php echo $base_url; ?>/static/humans.txt" />
        <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/css/foundation.css?v=<?php echo filemtime(getcwd() . '/assets/css/foundation.css'); ?>">
        <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/css/tweaks.css?v=<?php echo $cssVersion; ?>">
        <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/css/font-awesome.css?r=<?php echo time(); ?>">
