<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* * ***********************************************************************
  Document    : assets_helper
  Créé le     : 8 août 2011, 13:54:05
  Auteur      : Adrien Lopes
  Description :
  -  8 août 2011 Création
https://stackoverflow.com/questions/32913481/how-to-define-separate-url-for-assets-css-js-etc-like-base-url-in-codei
 * *********************************************************************** */


if ( ! function_exists('css'))
{
	function css($nom)
	{
		return '<link rel="stylesheet" href="' . base_url() . 'assets/css/' . $nom . '.css " type="text/css" media="screen" />';
	}
}

if ( ! function_exists('css_print'))
{
	function css_print($nom)
	{
		return '<link rel="stylesheet" href="' . base_url() . 'assets/css/' . $nom . '.css " type="text/css" media="print" />';
	}
}

if ( ! function_exists('jsdefer'))
{
	function js_defer($nom)
	{
		return '<script defer src="' . base_url() . 'assets/js/' . $nom . '.js" type="text/javascript" ></script>';
	}
}

if ( ! function_exists('js'))
{
	function js($nom)
	{
		return '<script src="' . base_url() . 'assets/js/' . $nom . '.js" type="text/javascript" ></script>';
	}
}

if ( ! function_exists('img_url'))
{
	function img_url($nom)
	{
		return base_url() . 'assets/images/' . $nom;
	}
}

if ( ! function_exists('img'))
{
	function img($nom, $alt = '', $width='', $class='', $onclick='')
	{
		return '<img src="' . img_url($nom) . '" alt="' . $alt . '" width="'.$width.'" class="'.$class.'" onclick="'.$onclick.'" />';
	}
}

if ( ! function_exists('js_app'))
{
	function js_app($appName,$jsName)
	{
		return '<script src="' . base_url() . 'application/modules/'.$appName.'/assets/js/' . $jsName . '.js" type="text/javascript"></script>';
	}
}

if ( ! function_exists('css_app'))
{
	function css_app($appName,$cssName)
	{
		return '<link rel="stylesheet" href="' . base_url() . 'application/modules/'.$appName.'/assets/css/' . $cssName . '.css " type="text/css" media="screen" />';
		
	}
}