<?php
require('includes/constant.php');
?>
<?php include $_SERVER["DOCUMENT_ROOT"] . "/includes/config.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <?php include $_SERVER["DOCUMENT_ROOT"] . "/includes/head.php"; ?>
	<script src='https://www.google.com/recaptcha/api.js'></script>	
</head>
<body>



<?php include $_SERVER["DOCUMENT_ROOT"] . "/views/templates/navigation.php"; ?>
<div class="panel">
    <div class="grid-x align-center">
<div id="central">
	<div class="content">
		<h1>Contact Form</h1>
		<p>Tulis pertanyaan anda disini</p>
		<div id="message">
		<fieldset class="fieldset">
  <legend>Isi data agar kami dapat menghubungi anda.</legend>
		<form id="frmContact" action="" method="POST" novalidate="novalidate">
			<div class="label">Name:</div>
			<div class="field">
				<input type="text" id="name" name="name" placeholder="enter your name here" title="Please enter your name" class="required" aria-required="true" required>
			</div>
			<div class="label">Email:</div>
			<div class="field">			
				<input type="text" id="email" name="email" placeholder="enter your email address here" title="Please enter your email address" class="required email" aria-required="true" required>
			</div>
			<div class="label">Phone Number:</div>
			<div class="field">			
				<input type="text" id="phone" name="phone" placeholder="enter your phone number here" title="Please enter your phone number" class="required phone" aria-required="true" required>
			</div>
			<div class="label">Company Name:</div>
			<div class="field">			
				<input type="text" id="company" name="company" placeholder="enter your company name here" title="Please enter your company name" class="required subject" aria-required="true" required>
			</div>
			<div class="label">Subject:</div>
			<div class="field">			
				<input type="text" id="subject" name="subject" placeholder="enter your subject here" title="Please enter your subject" class="required subject" aria-required="true" required>
			</div>
			<div class="label">Comments:</div>
			<div class="field">			
				<textarea id="comment-content" name="content" placeholder="enter your comments here"></textarea>			
			</div>
			<div class="g-recaptcha" data-sitekey="<?php echo SITE_KEY; ?>"></div>			
			<div id="mail-status"></div>
			<br>			
			<button class="button" type="Submit" id="send-message" style="clear:both;">Send Message</button>
		</form>
		</fieldset>
		<div id="loader-icon" style="display:none;"><img src="assets/img/loader.gif" /></div>
		</div>		
	</div><!-- content -->
</div><!-- central -->	
</div>
</div>

<?php include $_SERVER["DOCUMENT_ROOT"] . "/views/templates/footer.php"; ?>
<!--<script src="component/jquery/jquery-3.2.1.min.js"></script>-->
<script>


	var verifyCallback = function(response) {
    	$('.btn-submit').attr('data-response',response);
		$('.btn-submit').css('display','block')
    };
	var onloadCallback = function() {
		grecaptcha.render('captcha', {
			'sitekey' : '6LdJ-HEUAAAAAHu7ltxIPBMxtni-oMQ5AvDRKbwi',
			'callback': verifyCallback,
		});
	};











	$(document).ready(function (e){
		$("#frmContact").on('submit',(function(e){
			e.preventDefault();
			$("#mail-status").hide();
			$('#send-message').hide();
			$('#loader-icon').show();
			$.ajax({
				url: "sendmail.php",
				type: "POST",
				dataType:'json',
				data: {
				"name":$('input[name="name"]').val(),
				"email":$('input[name="email"]').val(),
				"phone":$('input[name="phone"]').val(),
				"company":$('input[name="company"]').val(),
				"subject":$('input[name="subject"]').val(),
				"content":$('textarea[name="content"]').val(),
				"g-recaptcha-response":$('textarea[id="g-recaptcha-response"]').val()},				
				success: function(response){
				$("#mail-status").show();
				$('#loader-icon').hide();
				if(response.type == "error") {
					$('#send-message').show();
					$("#mail-status").attr("class","error");				
				} else if(response.type == "message"){
					$('#send-message').hide();
					$("#mail-status").attr("class","success");							
				}
				$("#mail-status").html(response.text);	
				},
				error: function(){} 
			});
		}));
	});
	</script>
</body>
</html>
