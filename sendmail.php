<?php
// https://stackoverflow.com/questions/42533245/how-to-send-two-different-messages-to-two-different-users-phpmailer/42533474
// 2 email ke admin ke user
require('vendor/autoload.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/*
$message = file_get_contents('views/email.php');
$message = str_replace('%username%', $_POST["name"], $message);
*/

//Add the message header
//$message = file_get_contents('views/email/header.php');
$message = file_get_contents('views/email-templates/header.php');
    
//Add the message body
$message .= file_get_contents('views/email-templates/body.php');

//Add the message footer content
$message .= file_get_contents('views/email-templates/footer.php');

$pengirim = "support@okusi.co.id";
$logo = 'https://okusi.co.id/assets/img/okusi-infotech.png';
$logo2 ='https://okusi.co.id/assets/img/logo.png';

$mail = new PHPMailer();
$mail->IsSMTP();
$mail->SMTPDebug  = 0;
$mail->SMTPAuth   = TRUE;
$mail->SMTPSecure = "tls";
$mail->Port       = 587;
$mail->Username   = $pengirim;
$mail->Password   = "";
$mail->Host       = "";
$mail->Mailer     = "smtp";
//$mail->SetFrom($_POST["email"], $_POST["name"]);
$mail->SetFrom('support@okusi.co.id', 'Okusi Infotech');
//$mail->AddReplyTo($_POST["email"], $_POST["name"]);
//$mail->AddReplyTo('hasanudin@okusi.id', 'Admin');
$mail->AddAddress($_POST["email"], $_POST["name"]);
$mail->AddBCC($pengirim);
//$mail->AddAddress($pengirim);
//$mail->Subject = $_POST["phone"];
$mail->Subject  = $_POST["subject"];
$mail->Subject .= ' - ';
$mail->Subject .= $_POST["company"];
$mail->Subject .= ' - ';
$mail->Subject .= date("Y-m-d H-i-s");
$mail->WordWrap = 80;
//$mail->MsgHTML($_POST["content"]);
$mail->MsgHTML($message);
$mail->setLanguage('id', 'vendor/phpmailer/phpmailer/language/');
$mail->CharSet="utf-8";
$mail->IsHTML(true);


// validasi form

if ($_POST)
  {
    require('includes/constant.php');
    
    $name    = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    $email   = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $phone   = filter_var($_POST["phone"], FILTER_SANITIZE_NUMBER_FLOAT);
    $company    = filter_var($_POST["company"], FILTER_SANITIZE_STRING);
    $subject = filter_var($_POST["subject"], FILTER_SANITIZE_STRING);
    $content      = filter_var($_POST["content"], FILTER_SANITIZE_STRING);
    
    if (empty($name))
      {
        $empty[] = "<b>Name</b>";
      }
    if (empty($email))
      {
        $empty[] = "<b>Email</b>";
      }
    if (empty($phone))
      {
        $empty[] = "<b>Phone Number</b>";
      }
    if (empty($subject))
      {
        $empty[] = "<b>Subject</b>";
      }
    if (empty($content))
      {
        $empty[] = "<b>Comments</b>";
      }
    
    if (!empty($empty))
      {
        $output = json_encode(array(
            'type' => 'error',
            'text' => implode(", ", $empty) . ' Required!'
        ));
        die($output);
      }
    if (!filter_var($phone, FILTER_VALIDATE_FLOAT))
      {
        $output = json_encode(array(
            'type' => 'error',
            'text' => '<b>' . $phone . '</b> is an invalid Phone Number, please correct it.'
        ));
        die($output);
      }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) //email validation
      {
        $output = json_encode(array(
            'type' => 'error',
            'text' => '<b>' . $email . '</b> is an invalid Email, please correct it.'
        ));
        die($output);
      }
    
    //reCAPTCHA validation
    if (isset($_POST['g-recaptcha-response']))
      {
        
        require('vendor/autoload.php');
        
        $recaptcha = new \ReCaptcha\ReCaptcha(SECRET_KEY, new \ReCaptcha\RequestMethod\SocketPost());
        
        $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        
        if (!$resp->isSuccess())
          {
            $output = json_encode(array(
                'type' => 'error',
                'text' => '<b>Captcha</b> Validation Required!'
            ));
            die($output);
          }
      }
    

    

//Replace the codetags with the message contents
$replacements = array(
  '({logo})' => $logo,
  '({logo2})' => $logo2,
  '({name})' => $name,
  '({phone})' => $phone,
  '({company})' => $company,
  '({message_subject})' => $subject, 
  '({message_body})' => nl2br( stripslashes( $content ) )
);
$message = preg_replace( array_keys( $replacements ), array_values( $replacements ), $message );

//Make the generic plaintext separately due to lots of css and tables
$plaintext = $content;
$plaintext = strip_tags( stripslashes( $plaintext ), '<p><br><h2><h3><h1><h4>' );
$plaintext = str_replace( array( '<p>', '<br />', '<br>', '<h1>', '<h2>', '<h3>', '<h4>' ), PHP_EOL, $plaintext );
$plaintext = str_replace( array( '</p>', '</h1>', '</h2>', '</h3>', '</h4>' ), '', $plaintext );
$plaintext = html_entity_decode( stripslashes( $plaintext ) );

//Send the message as HTML
$mail->MsgHTML( stripslashes( $message ) ); 
//Set the plain text version just in case
$mail->AltBody = $plaintext;



    
    if (!$mail->Send())
      {
        $output = json_encode(array(
            'type' => 'error',
            'text' => 'Unable to send email, please contact ' . $pengirim
        ));
        die($output);
      }
    else
      {
        $output = json_encode(array(
            'type' => 'message',
            'text' => 'Hi ' . $name . ', thank you for the comments. We will get back to you shortly.'
        ));
        die($output);
      }
    
    
    
  }


?> 
