#!/bin/bash
#Content-Type: text/plain; charset=\"US-ASCII\"
#Content-Disposition: attachment; filename=\"$file\"
##Content-Type: text/html; charset=\"ISO-8859-1\"
## https://stackoverflow.com/questions/16242489/send-a-base64-image-in-html-email
# Content-Type: multipart/related; boundary=\"$boundary\"

function get_mimetype(){
file --mime-type "$1" | sed 's/.*: //'
}

from="Okusi Infotech<info@okusi.co.id>"
to="$1"
subject="Layanan IT Okusi ${RANDOM}"
#boundary="=== Boundary ==="
#boundary="${RANDOM}_${RANDOM}_${RANDOM}"
boundary="------------$(date | md5sum | cut -d ' ' -f 1 | tr '/a-z/' '/A-Z/')"
#body="$(cat cidtemplate.html)"
#body="$(<cidtemplate)"
body="$(<OkusiInfotech.html)"
#body="ini coba dkim"
declare -a attachments
#attachments=( "fileOne.out" "fileTwo.out" "fileThree.out" "file-et-cetera.out")
attachments=( "brosur3.jpg" "logo.png" )

# Build headers
{
printf '%s\n' "From: $from
To: $to
Subject: $subject
Mime-Version: 1.0
Content-Type: multipart/alternative; boundary=\"$boundary\"

--${boundary}
Content-Type: text/html; charset=\"UTF-8\"
Content-Type: text/plain; charset=\"US-ASCII\"
Content-Transfer-Encoding: 7bit
Content-Disposition: inline

$body
"

for file in "${attachments[@]}"; do

      [ ! -f "$file" ] && echo "Attachment $file not found, omitting file" >&2 && continue

        mimetype=$(get_mimetype "$file")
	namecut=$(ls $file | cut -d '.' -f 1)
  printf '%s\n' "--${boundary}
Content-Type: $mimetype; name=\"$file\"
Content-Transfer-Encoding: base64
Content-ID: <$namecut>
Content-Disposition: inline; filename=\"$file\"
  "

  base64 "$file"
  echo
done

# print last boundary with closing --
printf '%s\n' "--${boundary}--"

} | sendmail -t -oi
