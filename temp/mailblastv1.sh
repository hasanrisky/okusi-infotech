# sends email using sendmail
# usage: echo "my email message" | email "foo@bar.com" "new event happened" "serverX" "noreply@serverx.com"
#Date: Wed, 31 Oct 2018 11:53:19 +0700
#To: hasanrisky@gmail.com, hasanudin@okusi.id
#Subject: Layanan IT Profesional Linux Test Mail
#From: Okusi Infotech <info@okusi.co.id>
#Content-Type: text/html
#User-Agent: s-nail v14.8.6
#MIME-Version: 1.0
#Content-Type: text/plain; charset=US-ASCII
#Content-Transfer-Encoding: quoted-printable



penerima="$1"
subjek="$2"
nama="Infotech"
pengirim="info@okusi.co.id"
tipehtml="text/html"
mime="1.0"
multitipe="multipart/alternative; " 

msg=' 
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: ANNOUNCE -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Okusi Infotech</title>

    <style type="text/css">
        #outlook a,h1,h2,h3,h4,h5,h6,p{padding:0}h1,h2{color:#222}h2,h3{text-align:left}h1,h4{text-align:center}p{margin:10px 0}table{border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0}h1,h2,h3,h4,h5,h6{display:block;margin:0}a img,img{border:0;height:auto;outline:0;text-decoration:none}#bodyCell,#bodyTable,body{height:100%;margin:0;padding:0;width:100%}.mcnPreviewText{display:none!important}img{-ms-interpolation-mode:bicubic}.ExternalClass,.ReadMsgBody{width:100%}a,blockquote,li,p,td{mso-line-height-rule:exactly}a[href^=sms],a[href^=tel]{color:inherit;cursor:default;text-decoration:none}a,blockquote,body,li,p,table,td{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inh
 erit!important}h1,h2,h3{font-family:Helvetica;font-style:normal;font-weight:700;line-height:150%;letter-spacing:normal}.templateContainer{max-width:600px!important}a.mcnButton{display:block}.mcnImage,.mcnRetinaImage{vertical-align:bottom}.mcnTextContent{word-break:break-word}.mcnTextContent img{height:auto!important}.mcnDividerBlock{table-layout:fixed!important}h1{font-size:40px}h2{font-size:34px}h3{color:#444;font-size:22px}h4{color:#999;font-family:Georgia;font-size:20px;font-style:italic;font-weight:400;line-height:125%;letter-spacing:normal}#templateHeader{background-color:#fff;background-image:none;background-repeat:no-repeat;background-position:50% 50%;background-size:contain;border-top:0;border-bottom:0;padding-top:30px;padding-bottom:20px}#templateBody,#templateFooter,.bodyContainer,.footerContainer,.headerContainer{background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0}.headerContainer{background-color
 :#transparent;padding-top:0;padding-bottom:0}.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{color:grey;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left}.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{color:#00ADD8;font-weight:400;text-decoration:underline}#templateBody{background-color:#fff;padding-top:5px;padding-bottom:54px}.bodyContainer{background-color:#transparent;padding-top:0;padding-bottom:0}.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{color:grey;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left}.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{color:#00ADD8;font-weight:400;text-decoration:underline}#templateFooter{background-color:#1a1a1a;padding-top:15px;padding-bottom:15px}.footerContainer{background-color:#transparent;padding-top:0;padding-bottom:0}.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{color:#FFF;font-family:Helvetica;f
 ont-size:12px;line-height:150%;text-align:center}.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{color:#FFF;font-weight:400;text-decoration:underline}@media only screen and (min-width:768px){.templateContainer{width:600px!important}}@media only screen and (max-width:480px){a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:none!important}body{width:100%!important;min-width:100%!important}.mcnRetinaImage{max-width:100%!important}.mcnImage{width:100%!important}.mcnBoxedTextContentContainer,.mcnCaptionBottomContent,.mcnCaptionLeftImageContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightImageContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionTopContent,.mcnCartContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightImageContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageGroupContentContainer,.mcnRecContentContainer,.mcnTextContentContainer{max-width:100%!important;
 width:100%!important}.mcnBoxedTextContentContainer{min-width:100%!important}.mcnImageGroupContent{padding:9px!important}.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{padding-top:9px!important}.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnImageCardTopImageContent{padding-top:18px!important}.mcnImageCardBottomImageContent{padding-bottom:9px!important}.mcnImageGroupBlockInner{padding-top:0!important;padding-bottom:0!important}.mcnImageGroupBlockOuter{padding-top:9px!important;padding-bottom:9px!important}.mcnBoxedTextContentColumn,.mcnTextContent{padding-right:18px!important;padding-left:18px!important}.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{padding-right:18px!important;padding-bottom:0!important;padding-left:18px!important}.mcpreview-image-uploader{display:none!important;width:100%!important}h1{font-size:30px!important;line-heig
 ht:125%!important}h2{font-size:26px!important;line-height:125%!important}h3{font-size:20px!important;line-height:150%!important}h4{font-size:18px!important;line-height:150%!important}.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{font-size:14px!important;line-height:150%!important}.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p,.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{font-size:16px!important;line-height:150%!important}.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{font-size:14px!important;line-height:150%!important}}     
    </style>
   </head>
    <body>
        <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Solusi IT - </span><!--<![endif]-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top" id="templateHeader" data-template-container>
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="headerContainer"><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
                                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                <![endif]-->

                                <!--[if mso]>
                                <td valign="top" width="600" style="width:600px;">
                                <![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>

                        <td class="mcnTextContent" style="padding: 0px 18px 9px;color: #808080;font-style: normal;font-weight: normal;" valign="top">

                            <h4><span style="font-family:times new roman,times,baskerville,georgia,serif">Hallo, Perkenalkan kami</span></h4>

<h1><img data-file-id="220021" src="https://okusi.co.id/assets/img/okusi-infotech.png" style="border: 0px none; margin: 0px;" width="39" height="39"> <span style="color:#333333; font-family:kabel BK BT,STHeiti,sans-serif;">Okusi Infotech</span></h1>

                        </td>
                    </tr>
                </tbody></table>
                                <!--[if mso]>
                                </td>
                                <![endif]-->

                                <!--[if mso]>
                                </tr>
                                </table>
                                <![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px 10px;">
                <table class="mcnDividerContent" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--           
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 1px solid #E0E0E0;" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--           
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateBody" data-template-container>
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="bodyContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">

                                    <a href="https://okusi.co.id" title="" class="" target="_blank">
                                        <img alt="" src="https://okusi.co.id/assets/img/brosur3.jpg" style="max-width:1280px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="564" align="middle">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
                                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                <![endif]-->

                                <!--[if mso]>
                                <td valign="top" width="600" style="width:600px;">
                                <![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>

                        <td class="mcnTextContent" style="padding: 0px 18px 9px;color: #808080;font-style: normal;font-weight: normal;" valign="top">

                            <h3>Kami dapat membantu dalam mengoptimalkan layanan IT di bisnis Anda.</h3>

<p style="color: #808080;font-style: normal;font-weight: normal;">Kami berkomitmen membawa solusi sistem IT &amp; memberikan pelayanan terbaik bagi kemajuan bisnis &amp; usaha anda berbasis teknologi Open Source.<br>
Berikut ragam layanan servis kami :</p>

                        </td>
                    </tr>
                </tbody></table>
                                <!--[if mso]>
                                </td>
                                <![endif]-->

                                <!--[if mso]>
                                </tr>
                                </table>
                                <![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
        <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
        <![endif]-->
        <tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">

                                <!--[if gte mso 9]>
                                <td align="center" valign="top" width="300">
                                <![endif]-->
                <table class="mcnBoxedTextContentContainer" width="300" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>

                        <td class="mcnBoxedTextContentColumn" style="padding-top:0px; padding-right:18px; padding-bottom:2px; padding-left:18px;">

                            <table class="mcnTextContentContainer" style="min-width:100% !important;" width="100%" cellspacing="0" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="padding: 10px;color: #808080;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, Verdana, sans-serif;font-size: 13px;font-weight: normal;text-align: left;" valign="top">
                                        <ul>
        <li style="text-align: left;">Jaringan Internet, Intranet &amp; Wifi.</li>
        <li style="text-align: left;">Domain, Website, &amp; Email.</li>
        <li style="text-align: left;">Server File &amp; Dokumen.</li>
        <li style="text-align: left;">Jaringan Telpon &amp; PABX</li>
        <li style="text-align: left;">Sharing Printer &amp; Scanner</li>
        <li style="text-align: left;">Sistem Keamanan</li>
</ul>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                                <!--[if gte mso 9]>
                                </td>
                                <![endif]-->

                                <!--[if gte mso 9]>
                                <td align="center" valign="top" width="300">
                                <![endif]-->
                <table class="mcnBoxedTextContentContainer" width="300" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>

                        <td class="mcnBoxedTextContentColumn" style="padding-top:0px; padding-right:18px; padding-bottom:2px; padding-left:18px;">

                            <table class="mcnTextContentContainer" style="min-width:100% !important;" width="100%" cellspacing="0" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="padding: 10px;color: #808080;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, Verdana, sans-serif;font-size: 13px;font-weight: normal;text-align: left;" valign="top">
                                        <ul>
        <li style="text-align: left;">Sistem CCTV</li>
        <li style="text-align: left;">Sistem Backup File &amp; Data</li>
        <li style="text-align: left;">Sistem Absensi</li>
        <li style="text-align: left;">Sistem Invoice dan E-Faktur</li>
        <li style="text-align: left;">Pengembangan Solusi Khusus</li>
</ul>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                                <!--[if gte mso 9]>
                                </td>
                                <![endif]-->

                                <!--[if gte mso 9]>
                </tr>
                </table>
                                <![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
        <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
        <![endif]-->
        <tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">

                                <!--[if gte mso 9]>
                                <td align="center" valign="top" ">
                                <![endif]-->
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table class="mcnTextContentContainer" style="min-width:100% !important;" width="100%" cellspacing="0" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="padding: 18px;color: #808080;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, Verdana, sans-serif;font-size: 16px;font-weight: normal;text-align: left;" valign="top">
                                        <div style="text-align: center;">Sudahkah anda mengoptimalkan sistem IT untuk bisnis anda?</div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                                <!--[if gte mso 9]>
                                </td>
                                <![endif]-->

                                <!--[if gte mso 9]>
                </tr>
                </table>
                                <![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px 20px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 1px solid #E0E0E0;" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--           
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #FF6600;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" valign="middle" align="center">
                                <a class="mcnButton " title="Selengkapnya" href="https://okusi.co.id" target="_blank" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Selengkapnya</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateFooter" data-template-container>
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="footerContainer"><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
                                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                <![endif]-->

                                <!--[if mso]>
                                <td valign="top" width="600" style="width:600px;">
                                <![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>

                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">

                            &copy; 2008-2018 PT. Dharma Okusi.
                        </td>
                    </tr>
                </tbody></table>
                                <!--[if mso]>
                                </td>
                                <![endif]-->

                                <!--[if mso]>
                                </tr>
                                </table>
                                <![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
'

email(){
  content="$msg"; email="$penerima"; subject="$subjek"; fromname="$nama"; from="$pengirim"
  {
    echo "Subject: $subject";
    echo "From: $fromname <$from>";
    echo "To: $email";
    echo "MIME-Version: $mime";
    echo "Content-Type: $tipehtml";
    echo ""
    echo "$msg";
  } | $(which sendmail) -F "$from" "$email"
}
email
