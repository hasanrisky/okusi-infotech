<?php
  session_start();
 
  //Define Language file paths
  define("LANG_ID_PATH", $_SERVER['DOCUMENT_ROOT'] . '/includes/lang/id/');
  define("LANG_EN_PATH", $_SERVER['DOCUMENT_ROOT'] . '/includes/lang/en/');
 
 
  if (isset($_GET['lang'])) {
     
    // GET request found
 
    if ($_GET['lang'] == 'id') {
       
      // asked for the language 'id' so include the 'id.php' file
      include LANG_ID_PATH . '/id.php';
      $_SESSION['lang'] = 'id';
    } else {
 
      // if not asked for 'id', include 'en.php' as default
      include LANG_EN_PATH . '/en.php';
      $_SESSION['lang'] = 'en';
    }
  } 
  
  else if (isset($_SESSION['lang'])) {
 
    //SESSION variable found
 
    if ($_SESSION['lang'] == 'id') {
 
      // language already set to 'id', so include 'id.php'
      include LANG_ID_PATH . '/id.php';
    } else {
 
      // SESSION variable not set to 'id', so include 'en.php' by default
      include LANG_EN_PATH . '/en.php';
    }
  } 
  
  else {
     
    // SESSION varibale not set, so set it to 'en' and include 'en.php' by default
    include LANG_EN_PATH . 'en.php';
    $_SESSION['lang'] = 'en';
  }
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/config.php');?>

<?php

if ($CURRENT_PAGE == "Index") {
  $base_url      = $protocol . $_SERVER['HTTP_HOST'];


}

else {

  $base_url      = $protocol . $_SERVER['HTTP_HOST'];

}
?>

<?php $cssVersion = "3.4.2"; ?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Hmm, Anda salah Jalan Cowboy ! <?php echo " | Status " . http_response_code();?></title>
  <meta name="description" content="Salah Jalan!">
  <meta name="author" content="hasanrisky" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="icon" href="<?php echo $base_url; ?>/assets/img/okusi-infotech.svg">
  <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/css/foundation<?php echo time(); ?>.css?v=<?php echo filemtime(getcwd() . '/assets/css/foundation.css'); ?>">
  <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/css/tweaks.css?v=<?php echo $cssVersion; ?>">
  <link rel="stylesheet" media="screen" href="<?php echo $base_url; ?>/assets/css/404.css">
</head>
<body>


<!-- particles.js container -->
<div id="particles-js">
   
</div>
<div class="grid-x align-center-middle">
<div class="large-8 columns callout text-center align-self-middle" data-closable="slide-out-right">
  <button class="close-button" data-close>&times;</button>
  <h5>Hmm Cowboy</h5>
  <p>Anda Salah jalan, tidak ada apa-apa disini. <?php echo "  Status Not Found " . http_response_code();?></p>
  <a href="<?php echo $base_url; ?>">Kembali kejalan yang benar...</a>
</div>
</div>  
<!-- scripts -->
<script src="<?php echo $base_url; ?>/assets/js/vendor/particles.js"></script>
<script src="<?php echo $base_url; ?>/assets/js/vendor/app.js"></script>
</body>
</html>