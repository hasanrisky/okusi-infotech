<h3 style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 22px; margin: 0; padding: 0;" align="left">
    Hi {name}, Terimakasih telah menghubungi kami.
</h3>
<br>
<h5 style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 20px; margin: 0; padding: 0;" align="left">
Subjek pertanyaan {message_subject},
</h5>
<br>
<p style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 18px; margin: 0 0 10px; padding: 0;" align="left">
Mengenai isi pertanyaan anda tentang {message_body}, kami akan segera menjawabnya dan menguhubungi anda.
</p>
<br>
<br>
<p style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 18px; margin: 0 0 10px; padding: 0;" align="left">
Info Anda :
</p>
<p style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; margin: 0 0 10px; padding: 0;" align="left">
Nama Perusahaan : {company}
<br>
Nomor Telepon   : {phone}
</p>
<br>