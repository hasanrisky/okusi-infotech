<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125389035-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125389035-1');
</script>

<div class="orbit clean-hero-slider" role="region" aria-label="" data-orbit>
  <div class="orbit-wrapper">
    <div class="orbit-controls">
      <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
      <button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
    </div>
    <ul class="orbit-container">

      <li class="orbit-slide" data-slide="0">
        <figure class="orbit-figure">
          <img class="orbit-image" src="<?php echo $base_url; ?>/assets/img/laptop-new-2000x7501st.png" alt="Ubuntu Desktop">
          <figcaption class="orbit-caption">
          <?php echo $LANG['slide1'];?>
          </figcaption>
        </figure>
      </li>

      <li class="orbit-slide" data-slide="1">
        <figure class="orbit-figure">
          <img class="orbit-image" src="<?php echo $base_url; ?>/assets/img/lan-2000x750.png" alt="Jaringan">
          <figcaption class="orbit-caption">
          <?php echo $LANG['slide2'];?>
          </figcaption>
        </figure>
      </li>

      <li class="orbit-slide" data-slide="2">
        <figure class="orbit-figure">
          <img class="orbit-image" src="<?php echo $base_url; ?>/assets/img/solusi-2000x750.png" alt="Solusi IT">
          <figcaption class="orbit-caption">
          <?php echo $LANG['slide3'];?>
          </figcaption>
        </figure>
      </li>

      <li class="is-active orbit-slide" data-slide="3">
        <figure class="orbit-figure">
          <img class="orbit-image" src="<?php echo $base_url; ?>/assets/img/server-new-2000x750.png" alt="Ubuntu Server">
          <figcaption class="orbit-caption">
          <?php echo $LANG['slide4'];?>
          </figcaption>
        </figure>
      </li>

    </ul>
  </div>
  <nav class="orbit-bullets">
    <button class="is-active" data-slide="0"><span class="show-for-sr">1</span></button>
    <button data-slide="1"><span class="show-for-sr">2</span></button>
    <button data-slide="2"><span class="show-for-sr">3</span></button>
    <button data-slide="3"><span class="show-for-sr">4</span></button>
  </nav>
</div>

<br>


<hr>
    



        <div class="text-center">
         <h3 class="feature-block-header"><?php echo $LANG['paketheader'];?>
<!-- <button class="clear button alert large rata" data-toggle="paketmodal" type="button" data-tooltip tabindex="1" title="Apa ini ?" data-position="top" data-alignment="center"><h3>?</h3></button> -->
</h3>
         <h4 class="subheader"><?php echo $LANG['paketsubheader'];?></h4>

      </div>
      <article class="grid-container">
         <div class="grid-x grid-margin-x align-center">
         

          <div class="small-12 medium-3 large-3 columns">
          <?php echo $LANG['paketpurwa'], $LANG['paketbeli'];?> 
           </div>


           <div class="small-12 medium-3 large-3 columns"><span class="asliribbon">Best Seller</span>
           <?php echo $LANG['paketmadya'], $LANG['paketbeli'];?>
           </div>
          
           <div class="small-12 medium-3 large-3 columns">
           <?php echo $LANG['paketutama'], $LANG['paketbeli'];?>   
           </div>

            



<div class="reveal" id="paketmodal" data-reveal data-close-on-click="true" data-animation-in="spin-in" data-animation-out="spin-out">
          <p class="text-left"></p><br>
          <h4 class="marketing-site-features-title">Paket-paket ini khusus untuk jasa perawatan IT secara menyeluruh selama masa kontrak, meliputi:</h4>
             <ul>
             <li>Hardware setup, Instalasi &amp; konfigurasi
                 <br>Melakukan setup server dari instalasi konfigurasi dan menyediakan service-service yang akan digunakan.
             </li><li>Manajemen Jaringan LAN dan Internet
               <br>Melakukan manajemen koneksi internet dari : 
               pembatasan quota dan bandwith internet masing-masing user 
               security jaringan lokal area network 
               pembatasan dan pengaturan website yang boleh diakses. 
             </li><li>Manajemen Domain (DNS lokal) 
             </li><li>Solusi, konsultasi dan implementasi keamanan Jaringan
               <br>Akan memberikan solusi terbaik untuk keamanan jaringan baik dari sisi operating sistem dan aplikasi yang digunakan  
               </li><li>Migrasi Windows ke Ubuntu Linux
               <br>Profesional dalam migrasi/perpindahan dari Operating System Microsoft Windows ke Operating System Ubuntu Linux 
             </li><li>Manajemen dan monitoring system
               <br>Menyediakan tools untuk monitoring sistem baik dari sisi aplikasi, jaringan dan dari sisi Hardware.  
             </li><li>Perawatan System selama Setahun
               <br>Melakukan perawatan sistem dalam jangka waktu satu tahun dan bisa diperpanjang dalam jangka waktu yang fleksibel 
             </li><li>Perawatan Perangkat Hardware.
               <br>Perawatan perangkat keras (hardware) baik dari sisi Server, PC, Printer dan alat yang terkait dengan sistem.
           </li><li>Desktop Manajemen
               <br>Manajemen user/grup dan Manajemen Direktori/filesharing sesuai dengan aturan yang berlaku pada perusahaan anda. 
           <p></p>
           <br>
           
           </li>
        </ul>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

      
         </div>

   
         
         
         <br>
         <div class="grid-x">
           
            <div class="small-12">
            <?php echo $LANG['paketheadline'];?>   
            </div>
         </div>
         <hr>







         <div id="Produk" class="text-center" data-magellan-target="Produk">
         <?php echo $LANG['produkheadersub'];?>   
         </div>
        
      <div class="grid-x grid-margin-x align-center">
        
        <div class="small-12 medium-3 large-4 columns">
          <div class="news-card-tag">
          
          </div>
          <div class="card news-card">
          <img src="<?php echo $base_url; ?>/assets/img/fd-vc-bayangan3-small.jpg">
          <div class="card-section">
          <div class="news-card-date"></div>
          <div class="news-card-article">
          <?php echo $LANG['produk1'];?>   
          </div>
          <div class="news-card-author align-center">
          <div class="news-card-author-image">
          
          </div>
          <div class="news-card-author-name">
              <a class="radius button" href="mailto:support@okusi.co.id?subject=Pesan Ubuntu Server Installer"><?php echo $LANG['produkbeli'];?></a>
                    
          </div>
          </div>
          </div>
          </div>
        </div>

        <div class="small-12 medium-3 large-4 columns">
            <div class="news-card-tag">
            </div>
            <div class="card news-card">
            <img src="<?php echo $base_url; ?>/assets/img/fd-vc-bayangan2-small.jpg">
            <div class="card-section">
            <div class="news-card-date"></div>
            <div class="news-card-article">
            <?php echo $LANG['produk2'];?>              
            </div>
            <div class="news-card-author align-center">
            <div class="news-card-author-image">
            
            </div>
            <div class="news-card-author-name">
                <a class="radius button" href="mailto:support@okusi.co.id?subject=Pesan Ubuntu Desktop Installer"><?php echo $LANG['produkbeli'];?></a>
                      
            </div>
            </div>
            </div>
            </div>
          </div>

<!--
          <div class="small-12 medium-3 large-4 columns">
              <div class="news-card-tag">
              </div>
              <div class="card news-card">
              <img src="<?php echo $base_url; ?>/assets/img/fd-vc-bayangan1-small.jpg">
              <div class="card-section">
              <div class="news-card-date"></div>
              <article class="news-card-article">
              <?php echo $LANG['produk3'];?>                
              </article>
              <div class="news-card-author align-center">
              <div class="news-card-author-image">
              
              </div>
              <div class="news-card-author-name">
                  <a class="radius button" href="mailto:support@okusi.co.id?subject=Pesan Okusi Business Desktop"><?php echo $LANG['produkbeli'];?></a>
                        
              </div>
              </div>
              </div>
              </div>
           </div>

-->



      </div>

         <hr>


         


            <section id="Servis" class="marketing-site-three-up">
              
              <?php echo $LANG['servisheader'];?>
               <br><br>
                <div class="grid-x align-center medium-unstack grid-margin-y">
                  <div class="small-12 medium-2 large-2 columns">
                     <i class="fa fa-globe" aria-hidden="true"></i>
                     <?php echo $LANG['servis1'];?>
                  </div>
                  <div class="small-12 medium-2 large-2 columns">
                     <i class="fa fa-user-o" aria-hidden="true"></i>
                     <?php echo $LANG['servis2'];?>
                  </div>
                  <div class="small-12 medium-2 large-2 columns">
                     <i class="fa fa-server" aria-hidden="true"></i>
                     <?php echo $LANG['servis3'];?>
                  </div>
                  <div class="small-12 medium-2 large-2 columns">
                    <i class="fa fa-fax" aria-hidden="true"></i>
                    <?php echo $LANG['servis4'];?>
                 </div>
               </div>

               <h2 class="marketing-site-three-up-headline"></h2>

               <div class="grid-x align-center medium-unstack grid-margin-y">
                <div class="small-12 medium-2 large-2 columns">
                   <i class="fa fa-print" aria-hidden="true"></i>
                   <?php echo $LANG['servis5'];?>
                </div>
                
              

                <div class="small-12 medium-2 large-2 columns">
                    <i class="fa fa-shield" aria-hidden="true"></i>
                    <?php echo $LANG['servis6'];?>
                 </div>


                <div class="small-12 medium-2 large-2 columns">
                  <i class="fa fa-camera" aria-hidden="true"></i>
                  <?php echo $LANG['servis7'];?>
               </div>

		
		<div class="small-12 medium-2 large-2 columns">
                    <i class="fa fa-database" aria-hidden="true"></i>
                    <?php echo $LANG['servis8'];?>
                 </div>


             </div>

            </section>
      
         


         <div id="Apps" class="marketing-site-features">
         <?php echo $LANG['servisappheader'];?>
          <div class="grid-x align-center">
            
            
            <div class="small-12 medium-3 large-3 columns">
              <i class="fa fa-check-square-o" aria-hidden="true"></i>
              <?php echo $LANG['servisapp1'];?>
            </div>

           

            <div class="small-12 medium-3 large-3 columns">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
              <?php echo $LANG['servisapp2'];?>
            </div>
            <div class="small-12 medium-3 large-3 columns">
              <i class="fa fa-connectdevelop" aria-hidden="true"></i>
              <?php echo $LANG['servisapp3'];?>
            </div>
            

          </div>
        </div>



     
      </article>
      
      
      <hr>
      <?php echo $LANG['klienheader'];?>
      <!-- klien -->
      <div class="ecommerce-product-slider orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container">
        
	<li class="is-active orbit-slide">
            <div class="grid-x small-up-2 medium-up-4 large-up-5 align-center">
              
            
            <div class="column">
              <div class="product-card">
                <div class="product-card-thumbnail">
                  <a href="http://jasacendekia.co.id/" target="_blank"><img src="<?php echo $base_url; ?>/assets/img/JCI2.png"/></a>
                </div>
                
                
              </div>
            </div>
            
            

             
          </li>

          
          <li class="orbit-slide">
            <div class="grid-x small-up-2 medium-up-4 large-up-5 align-center">
              

          
              <div class="column">
                <div class="product-card">
                  <div class="product-card-thumbnail">
                    <a href="#"><img src="<?php echo $base_url; ?>/assets/img/Tasply2-180x180.png"/></a>
                  </div>
                  
                </div>
              </div>
              

          
          </li>


          <li class="orbit-slide">
            <div class="grid-x small-up-2 medium-up-4 large-up-5 align-center">
              

          
              
              <div class="column">
                <div class="product-card">
                  <div class="product-card-thumbnail">
                    <a href="#"><img src="<?php echo $base_url; ?>/assets/img/puskomas.png"/></a>
                  </div>
                  
                </div>
              </div>

          
          </li>
          
       </ul>

<!--
        <nav class="orbit-bullets">
          <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
          <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
        </nav>
-->
      </div>
