<!-- 
<footer id="tos" class="callout rapat large secondary">
         <article class="grid-container">
            <div class="grid-x">
               
              
               <div class="columns small">
			
               <?php echo $LANG['ketentuan'];?>

                </div>


               

            </div>
         </article>
      </footer>

-->


    <footer class="marketing-site-footer">
      <div id="Kontak" class="grid-x medium-unstack grid-container">
        <div class="medium-4 columns">
        <?php echo $LANG['info'];?>
        <!--
           <ul class="menu marketing-site-footer-menu-social simple">
            <li><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
             <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
             <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
          </ul>
        -->
        </div>
        <div class="medium-4 columns">
           <h4 class="marketing-site-footer-title"><?php echo $LANG['navkontak'];?></h4>
          <div class="marketing-site-footer-block">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <?php echo $LANG['alamat1'];?>
          </div>
          <div class="marketing-site-footer-block">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <p>(021) 390-1007</p>
          </div>
          <div class="marketing-site-footer-block">
            <i class="fa fa-whatsapp" aria-hidden="true"></i>
            <p>(+62) 817-177-745</p>
          </div>
          <div class="marketing-site-footer-block">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <p>support@okusi.co.id</p>
          </div>
        </div>

        <div class="medium-4 columns">
        <?php echo $LANG['peta'];?>
          <div class="grid-x small-up-3 align-center">
            <!-- <div class="column column-block">
              
             
              
-->
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d991.6447598781514!2d106.82370182922595!3d-6.18704733483816!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7c3a05b23a1bfef!2sMenara+Cakrawala!5e0!3m2!1sen!2sus!4v1536551913633" width="280" height="200" frameborder="0" style="border:0"></iframe>


<!--
            </div>
          -->        
          </div>

        </div>
      </div><br>
      <div class="marketing-site-footer-bottom">
        <div class="grid-x large-unstack align-middle">
          <div class="large-6 columns">
            <p>&copy; 2008-<?php print date("Y");?> PT. Dharma Okusi. </p>
          </div>
          <div class="large-6 columns">
            <ul class="menu marketing-site-footer-bottom-links" data-magellan data-animation-duration="1200" data-animation-easing="swing">
              <li><a href="<?php echo $base_url; ?>#"><?php echo $LANG['navhome'];?></a></li>
              <li><a href="<?php echo $base_url; ?>#Servis"><?php echo $LANG['navservis'];?></a></li>
              <li><a href="<?php echo $base_url; ?>#Produk"><?php echo $LANG['navproduk'];?></a></li>
              <li><a href="/tentang"><?php echo $LANG['navtentang'];?></a></li>
              <li class="<?php if ($_SESSION['lang'] == 'id') {?>hide<?php }?>"><a href="id">Id</a></li>
              <li class="<?php if ($_SESSION['lang'] == 'en') {?>hide<?php }?>"><a href="en">En</a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    
    



      <script src="<?php echo $base_url; ?>/assets/js/vendor/jquery.js"></script>
      <script src="<?php echo $base_url; ?>/assets/js/vendor/what-input.js"></script>
      <script src="<?php echo $base_url; ?>/assets/js/vendor/foundation.js"></script>
      <script src="<?php echo $base_url; ?>/assets/js/app.js"></script>
      <script>
         $(document).foundation();
      </script>
